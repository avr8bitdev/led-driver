/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_LED_DRIVER_LED_FUNCTIONS_H_
#define HAL_DRIVERS_LED_DRIVER_LED_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

typedef struct
{
	DIO_port_t portLED; // LED port
	u8 pinLED; // LED pin

	u8 LED_off_state; // buzzer off state: 1, 0
} LED_obj_t;

typedef const LED_obj_t* LED_cptr_t;

void LED_vidInit(LED_cptr_t structPtrLEDCpy);

void LED_vidActivate(LED_cptr_t structPtrLEDCpy);
void LED_vidDeactivate(LED_cptr_t structPtrLEDCpy);

void LED_vidToggle(LED_cptr_t structPtrLEDCpy);

void LED_vidBlink(const u16 u16Timeout_msCpy, LED_cptr_t structPtrLEDCpy);


#endif /* HAL_DRIVERS_LED_DRIVER_LED_FUNCTIONS_H_ */

