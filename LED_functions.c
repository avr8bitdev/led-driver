/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "LED_functions.h"
#include <util/delay.h>


inline void LED_vidInit(LED_cptr_t structPtrLEDCpy)
{
	DIO_vidSet_pinDirection(structPtrLEDCpy->portLED, structPtrLEDCpy->pinLED, OUTPUT);
	DIO_vidSet_pinValue(structPtrLEDCpy->portLED, structPtrLEDCpy->pinLED, structPtrLEDCpy->LED_off_state);
}

inline void LED_vidActivate(LED_cptr_t structPtrLEDCpy)
{
	DIO_vidSet_pinValue(structPtrLEDCpy->portLED, structPtrLEDCpy->pinLED, !(structPtrLEDCpy->LED_off_state));
}

inline void LED_vidDeactivate(LED_cptr_t structPtrLEDCpy)
{
	DIO_vidSet_pinValue(structPtrLEDCpy->portLED, structPtrLEDCpy->pinLED, structPtrLEDCpy->LED_off_state);
}

inline void LED_vidToggle(LED_cptr_t structPtrLEDCpy)
{
	DIO_vidToggle_pinValue(structPtrLEDCpy->portLED, structPtrLEDCpy->pinLED);
}

inline void LED_vidBlink(const u16 u16Timeout_msCpy, LED_cptr_t structPtrLEDCpy)
{
	LED_vidActivate(structPtrLEDCpy);
	_delay_ms(u16Timeout_msCpy);
	LED_vidDeactivate(structPtrLEDCpy);
}

